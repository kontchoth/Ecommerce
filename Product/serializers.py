from .models import Category, Product
from rest_framework import serializers

class CategorySerializer(serializers.ModelSerializer):
	""" Product Serializer """
	class Meta:
		model = Category
		fields = ('id', 'name', 'created_at', 'updated_at')
		read_only_fields = ('id', 'created_at', 'updated_at')

class ProductSerializer(serializers.ModelSerializer):
	""" Product Serializer """
	category = serializers.StringRelatedField()
	class Meta:
		model = Product
		fields = ('id', 'category', 'name', 'price', 'quantity', 'description', 'created_at', 'updated_at')
		read_only_fields = ('id', 'created_at', 'updated_at')

	def create(self, validated_data):
		""" Create new Instance of Product """
		cat = Category.objects.get(name=validated_data['category'])
		validated_data['category'] = cat.id
		return Product(**validated_data)

	def update(self, instance, validated_data):
		""" Update Product information """
		cat = Category.objects.get(name=validated_data['category'])
		validated_data['category'] = cat.id
		instance.category = validated_data.get('category', instance.category)
		instance.name = validated_data.get('name', instance.name)
		instance.quantity = validated_data.get('quantity', instance.quantity)
		instance.price = validated_data.get('price', instance.price)
		return instance