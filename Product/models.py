from __future__ import unicode_literals

from django.utils.translation import ugettext as _
from django.db import models
from django.core.validators import MinValueValidator

# Create your models here.
class Category(models.Model):
	"""
	Representation of Product Category
		name - Name of the Category
		created_at - Date created
		updated_at - Date updated
	"""
	name = models.CharField(_('Name'), max_length=100, blank=False, null=False, unique=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		""" String representation """
		return self.name

class Product(models.Model):
	"""
	Representation of a Product
		category - Category the product belongs to
		name - name of the Product
		price - price of the product
		quantity - quantity of the product available
		description - Description of the Product
	"""
	category = models.ForeignKey(_('Category'), Category, null=False, blank=False)
	name = models.CharField(_('Product Name'), max_length=200, null=False, blank=False)
	price = models.FloatField(_('Product Price'), validators=[MinValueValidator(0.0)])
	quantity = models.IntegerField(_('Product Qty'), default=0, validators=[MinValueValidator(0)])
	description = models.TextField(default='No description')
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return '{} {}'.format(self.name, self.quantity)
